﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.IO;

namespace ParkingMain
{
    public class ParkLogicMediator
    {
        public static bool Execute { get; private set; } = true;

        private static readonly IParkingService _parkingService;

        public static void AddVehicleToParking()
        {
            Console.WriteLine("Please wright to vehicle type (PassengerCar,Truck,Bus,Motorcycle): ");
            string type = Console.ReadLine();
            VehicleType vehicleType;

            switch (type)
            {
                case "PassengerCar":
                    vehicleType = VehicleType.PassengerCar;
                    break;
                case "Truck":
                    vehicleType = VehicleType.Truck;
                    break;
                case "Bus":
                    vehicleType = VehicleType.Bus;
                    break;
                case "Motorcycle":
                    vehicleType = VehicleType.Motorcycle;
                    break;
                default:
                    throw new Exception("Not valid vehicle type. Please wright correct vehicle type.");
            }
            Console.WriteLine("Enter your vehicle balance: ");
            decimal balance = decimal.Parse(Console.ReadLine());
            var vehicle = new Vehicle(Guid.NewGuid().ToString(), vehicleType, balance);
            _parkingService.AddVehicle(vehicle);
            Console.WriteLine("Your vehicle has been successfully added.");
        }

        public static void RemoveVehicleFromParking()
        {
            Console.WriteLine("Input your vehicle id: ");
            string id = Console.ReadLine();
            _parkingService.RemoveVehicle(id);
            Console.WriteLine("Your vehicle has been removed.");
        }

        public static void AddVehicleBalance()
        {
            Console.WriteLine("Input your vehicle id: ");
            string id = Console.ReadLine();
            Console.WriteLine("Write sum what you want to put on balance : ");
            decimal amount = decimal.Parse(Console.ReadLine());
            _parkingService.TopUpVehicle(id, amount);
            Console.WriteLine("You added balance for your vehicle.");
        }

        public static void ReceiveVehiclesAmounts()
        {
            var trucksCount = Settings.GetVehicleValue(VehicleType.Truck);
            var busCount = Settings.GetVehicleValue(VehicleType.Bus);
            var motorcycleCount = Settings.GetVehicleValue(VehicleType.Motorcycle);
            var passengerCarCount = Settings.GetVehicleValue(VehicleType.PassengerCar);
            var allVehicles = trucksCount + busCount + motorcycleCount + passengerCarCount;
            Console.WriteLine("There are {0} vehicles on the parking.", allVehicles);
        }

        public static void ReceiveParkingVehicles()
        {
            Console.WriteLine("Parking: {0}", Parking.GetParking());
        }

        public static void ReceiveFreeParkingSpaces()
        {
            Console.WriteLine("For current time in parking there are {0} free spaces", Parking.GetParking().CurentBalance);
        }

        public static void ReceiveEarnedParkingMoney()
        {
            Console.WriteLine("Now parking earned money : {0}", _parkingService.GetBalance());
        }

        public static void ShowTransactionHistory()
        {
            var transactionsInfo = _parkingService.GetLastParkingTransactions();
            Console.WriteLine("Transactions information: {0}", transactionsInfo);
        }

        public static void ShowTrasactionFile()
        {
            Console.WriteLine("Write your transaction log : ");
            using (FileStream sr = File.OpenRead("Transaction.log"))
            {
                byte[] array = new byte[sr.Length];
                sr.Read(array, 0, array.Length);
                string transactions = System.Text.Encoding.Default.GetString(array);
                Console.WriteLine(transactions);
            }
        }

        public static void Exit()
        {
            Execute = false;
        }
    }
}
