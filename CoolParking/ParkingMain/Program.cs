﻿using System;

namespace ParkingMain
{
    class Program
    {
        static void Main(string[] args)
        {
            while (ParkLogicMediator.Execute == true)
            {
                try
                {
                    Console.WriteLine("Available commands: ");
                    Console.WriteLine("1. Display the current balance of the parking.");
                    Console.WriteLine("2. Display the amount of money earned for the current period.");
                    Console.WriteLine("3. Display the number of free/occupied parking spaces.");
                    Console.WriteLine("4. Display all parking transactions for the current period.");
                    Console.WriteLine("5. Display transaction history.");
                    Console.WriteLine("6. Display the list of Vehicles in the parking.");
                    Console.WriteLine("7. Put the Vehicle in the parking.");
                    Console.WriteLine("8. Pick up a vehicle from the parking");
                    Console.WriteLine("9. Top up the balance of a specific vehicle.");
                    Console.WriteLine("10. Exit.");
                    int command = Int32.Parse(Console.ReadLine());
                    switch (command)
                    {
                        case 1:
                            ParkLogicMediator.ReceiveVehiclesAmounts();
                            break;
                        case 2:
                            ParkLogicMediator.ReceiveEarnedParkingMoney();
                            break;
                        case 3:
                            ParkLogicMediator.ReceiveFreeParkingSpaces();
                            break;
                        case 4:
                            ParkLogicMediator.ShowTrasactionFile();
                            break;
                        case 5:
                            ParkLogicMediator.ShowTransactionHistory();
                            break;
                        case 6:
                            ParkLogicMediator.ReceiveParkingVehicles();
                            break;
                        case 7:
                            ParkLogicMediator.AddVehicleToParking();
                            break;
                        case 8:
                            ParkLogicMediator.RemoveVehicleFromParking();
                            break;
                        case 9:
                            ParkLogicMediator.AddVehicleBalance();
                            break;
                        case 10:
                            ParkLogicMediator.Exit();
                            break;
                        default:
                            throw new Exception("Not valid command");

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception: \n {0}", ex.Message);
                }
                finally
                {
                    Console.WriteLine("\n");
                }
            }
        }
    }
}
