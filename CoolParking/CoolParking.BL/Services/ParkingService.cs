﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Timers;
using static System.String;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private readonly Parking _parking;

        private readonly ILogService _logService;

        private readonly ITimerService _withdrawTimer;

        private readonly ITimerService _logTimer;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            _withdrawTimer = withdrawTimer ?? throw new ArgumentNullException(nameof(withdrawTimer));
            _logTimer = logTimer ?? throw new ArgumentNullException(nameof(logTimer));
            _logService = logService ?? throw new ArgumentNullException(nameof(logService));

            _parking = Parking.GetParking();

            _withdrawTimer.Interval = Settings.TimeOut;
            _logTimer.Interval = Settings.TimeCheck;
            _withdrawTimer.Elapsed += GetMoney;
            _logTimer.Elapsed += LogTransactions;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if (vehicle == null) throw new ArgumentNullException(nameof(vehicle));
            if (_parking.Vehicles.Count > Settings.ParkingSpace) throw new InvalidOperationException("Parking is full, please try later");

            if (!IsNotEqualId(vehicle.Id)) throw new ArgumentException("Vehicle with this id is staing on the parking");

            if (_parking.Vehicles.Count() == 0)
            {
                _withdrawTimer.Start();
                _logTimer.Start();
            }

            _parking.Vehicles.Add(vehicle);
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            var vehicle = _parking.Vehicles.FirstOrDefault();
            decimal cost = Settings.GetVehicleValue(vehicle.VehicleType);
            if (vehicle.Balance < cost) cost *= Settings.Fine;
            vehicle.Balance -= cost;
            _parking.Balance += cost;
        }
        private bool IsNotEqualId(string vehicleId)
        { 
            if (IsNullOrEmpty(vehicleId)) throw new ArgumentNullException(nameof(vehicleId));
            return !_parking.Vehicles.Any(vehicle => vehicle.Id == vehicleId); 
        }

        public decimal GetBalance() => _parking.Balance;
        
        public int GetCapacity() => Settings.ParkingSpace;
        
        public int GetFreePlaces() => Settings.ParkingSpace - _parking.Vehicles.Count();

        public TransactionInfo[] GetLastParkingTransactions() => _parking.TransactionInformation.ToArray();
        
        public ReadOnlyCollection<Vehicle> GetVehicles() => new ReadOnlyCollection<Vehicle>(_parking.Vehicles.ToList());
        
        public string ReadFromLog()
        {
            LogService log = new(Settings.LogPath);
            return log.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            if (!IsNotEqualId(vehicleId))
            {

                var vehicle = _parking.Vehicles.FirstOrDefault(v => v.Id == vehicleId);

                if (Vehicle.GetVehicleById(vehicleId).Balance < 0) throw new InvalidOperationException("You need to pay your debt");

                _parking.Vehicles.Remove(vehicle);

                if (_parking.Vehicles.Count() == 0)
                {
                    _withdrawTimer.Stop();
                    _logTimer.Stop();
                }

                return;
            }

            throw new ArgumentException("Id is not valid");
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (IsNullOrEmpty(vehicleId)) throw new ArgumentNullException(nameof(vehicleId));
            if (sum <= 0) throw new ArgumentException($"Your sum - {sum} is less or equal zero");

            Vehicle vehicle = Vehicle.GetVehicleById(vehicleId);

            if (vehicle != null)
            {
                vehicle.Balance += sum;
                Console.WriteLine("This vehicle have " + vehicle.Balance);
                return;
            }

            throw new ArgumentException($"Your id - {vehicleId} is not valid");
        }
        
        public void Dispose()
        {
            _parking.Dispose();
            _withdrawTimer.Dispose();
            _logTimer.Dispose();

            if (System.IO.File.Exists(Settings.LogPath))
                System.IO.File.Delete(Settings.LogPath);
            GC.Collect();
        }

        private void GetMoney(object sender, ElapsedEventArgs e)
        {
            var Vehicles = _parking.Vehicles;
            for (int i = 0; i < _parking.Vehicles.Count; i++)
            {
                var vehicleValue = Settings.GetVehicleValue(Vehicles[i].VehicleType);
                var newTransactionInfo = new TransactionInfo(System.DateTime.Now, Vehicles[i].Id, vehicleValue);

                if (Vehicles[i].Balance >= vehicleValue)
                {
                    _parking.TransactionInformation.Add(newTransactionInfo);
                    _parking.Balance += vehicleValue;
                    _parking.CurentBalance += vehicleValue;
                    Vehicles[i].Balance -= vehicleValue;
                    continue;
                }

                if (Vehicles[i].Balance > 0)
                {
                    _parking.TransactionInformation.Add(newTransactionInfo);
                    decimal NegativeValue = vehicleValue - Vehicles[i].Balance;
                    _parking.Balance += Vehicles[i].Balance;
                    _parking.CurentBalance += Vehicles[i].Balance;
                    Vehicles[i].Balance -= Vehicles[i].Balance + Settings.Fine * NegativeValue;
                    continue;
                }

                Vehicles[i].Balance -= Settings.Fine * vehicleValue;
            }           
        }

        private void LogTransactions(object sender, ElapsedEventArgs e)
        {
            var transaction = _parking.TransactionInformation.ToList();
            for (int i = 0; i < transaction.Count; i++)
            {
                LogOneTransaction(transaction[i]);
            }

            _logService.Write(string.Empty);
            _parking.TransactionInformation.Clear();
        }

        private void LogOneTransaction(TransactionInfo transaction) => _logService.Write($"{transaction.DateTime:T}\n{transaction.Id}\n{transaction.Sum}\n\n");
    }
}