﻿using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimeService : ITimerService
    {
        public event ElapsedEventHandler Elapsed;

        public double Interval { get; set; }

        private Timer _timer;

        public void Dispose()
        {
            _timer.Dispose();
        }

        public void Start()
        {
            _timer = new Timer(Interval);
            _timer.Elapsed += Elapsed;
            _timer.Start();
        }

        public void Stop()
        {
            _timer.Stop();
        }

        public void FireElapsedEvent()
        {
            Elapsed?.Invoke(this, null);
        }
    }
}