﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.
using System.Collections.Generic;

namespace CoolParking.BL.Models
{
    public class Parking
    {
        public decimal CurentBalance { get; set; } = Settings.Balance;

        public decimal Balance { get; set; } = Settings.Balance;

        public IList<TransactionInfo> TransactionInformation { get; set; }

        public IList<Vehicle> Vehicles { get; set; }     

        public Dictionary<int, bool> ParkingPlaces { get; set; }

        private static Parking _parking;

        private Parking()
        {
            ParkingPlaces = new Dictionary<int, bool>();
            for (int i = 0; i < Settings.ParkingSpace; i++)
                ParkingPlaces.Add(i, true);
            Vehicles = new List<Vehicle>();
            TransactionInformation = new List<TransactionInfo>();
        }

        public static Parking GetParking()
        {
            if (_parking == null) _parking = new Parking();
            return _parking;
        }

        public int GetFreeParkingPlace()
        {
            for (int i = 0; i < ParkingPlaces.Count; i++)
            {
                if (ParkingPlaces[i]) return i;
            }
            throw new System.InvalidOperationException("No free parking place");
        }

        public void Dispose()
        {
            _parking = null;
        }
    }
}