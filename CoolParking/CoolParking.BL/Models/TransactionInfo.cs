﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

public class TransactionInfo
{
    private string id;

    private decimal sum;

    public TransactionInfo(DateTime dateTime, string id, decimal sum)
    {
        DateTime = dateTime;
        Id = id;
        Sum = sum;
    }

    public DateTime DateTime { get; private set; }

    public string Id
    {
        get { return id; }
        private set
        {
            if (value.Length != 0)
                id = value;
            else throw new Exception("Need to exist vehicle id!");
        }
    }

    public decimal Sum
    {
        get { return sum; }
        private set
        {
            if (value >= 0)
                sum = value;
            else
                throw new Exception("Sum cannot be negative!");
        }
    }
}
