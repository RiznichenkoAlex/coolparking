﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.
using System;
using System.Collections.Generic;
using System.Linq;

namespace CoolParking.BL.Models
{
    public class Settings
    {
        public static decimal Balance { get; } = 0;

        public static decimal Fine { get; } = 2.5m;

        public static int TimeOut { get; } = 5000;

        public static int TimeCheck { get; } = 60000;

        public static int ParkingSpace { get; } = 10;

        static public string LogPath { get; set; } = @".\Transactions.log";

        public static Dictionary<VehicleType, decimal> VehiclesDict = new Dictionary<VehicleType, decimal>()
        {
            { VehicleType.PassengerCar, 2 },
            { VehicleType.Truck, 5 },
            { VehicleType.Bus, 3.5M },
            { VehicleType.Motorcycle, 1 }
        };

        public static decimal GetVehicleValue(VehicleType vehicleType)
        {
            return VehiclesDict.Where(x => x.Key == vehicleType).Select(x => x.Value).ToList().FirstOrDefault();
        }
    }
}